import inspect
from unittest.mock import sentinel


def test_call(fn_hub):
    val = fn_hub.mods.testing.echo(sentinel.param)
    assert val is sentinel.param


def test_signature(hub, fn_hub):
    assert inspect.signature(hub.pop.sub.add) == inspect.signature(fn_hub.pop.sub.add)
