import pytest


def test_logging(hub):
    hub.log.debug("Logging works")


@pytest.fixture(scope="function")
def function_hub(hub):
    hub.pop.sub.add(dyne_name="taco")
    yield hub


@pytest.fixture(scope="module")
def module_hub(hub):
    hub.pop.sub.add(dyne_name="salad")
    yield hub


def test_rescope(hub, function_hub, module_hub):
    # Changes to one hub changes all of them
    assert hasattr(function_hub, "taco")
    assert hasattr(function_hub, "salad")
